'use strict';

module.exports = (req, res, next) => {
    const { type, challenge, event } = req.body;

    if (type === 'url_verification') {
        res.json({ challenge });
    } else if (type === 'event_callback') {
        res.send('ok');

        if (event.type === 'message' && event.subtype !== 'bot_message') {
            next();
        }
    } else {
        res.json({
            ok: false,
            error: `${type} don't support`
        });
    }
};
