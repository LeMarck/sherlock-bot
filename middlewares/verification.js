'use strict';

module.exports = (req, res, next) => {
    if (req.body.token !== (process.env.SLACK_VERIFICATION_TOKEN)) {
        res.json({
            ok: false,
            error: `Token ${req.body.token} not verify`
        });
    } else {
        next();
    }
};
