# sherlock-bot

**Slack бот** для пересылки сообщений

## Env

* **SLACK_VERIFICATION_TOKEN** - Токен бота, для проверки входящих событий
* **SLACK_ACCESS_TOKEN** - Токен бота, для работы со Slack API
* **SLACK_CHANNEL_URL** - URL канала в который будет писать бот (`https://hooks.slack.com/services/xxx/yyy/zzz`)
* **SLACK_BOT_NAME** - имя бота (опционалоно);

## Usage

```sh
$ npm start

App listening on port 3000
```

## Deploy

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

## LICENSE

```
The MIT License (MIT)

Copyright (c) 2018-present Evgeny Petrov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```
