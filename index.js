'use strict';

const express = require('express');
const logger = require('morgan');

const verification = require('./middlewares/verification');
const proxy = require('./middlewares/proxy');

const im = require('./controllers/im');

if (process.env.NODE_ENV !== 'production') {
    require('./.env');
}

const PORT = process.env.PORT || 3000;

const app = express();

app.set('port', PORT);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.post('/', verification, proxy, im());

app.get('*', (req, res) => {
    res.send(`Hi! I'm ${process.env.SLACK_BOT_NAME || 'Slack Bot'}`);
});

app.listen(PORT, () => console.info(`App listening on port ${PORT}`));
