'use strict';

const request = require('request-promise-native');

module.exports = () => {
    let text;

    return async req => {
        const { event } = req.body;

        if (event.channel_type === 'im' && event.text !== text) {
            text = event.text;

            const user = await request.get('https://slack.com/api/users.info', {
                qs: {
                    token: process.env.SLACK_ACCESS_TOKEN,
                    user: event.user
                },
                json: true
            });

            const response = await request.post(process.env.SLACK_CHANNEL_URL, {
                json: { text: `[${decodeURIComponent(user.user.real_name)}]: ${event.text}` }
            });

            await request.post('https://slack.com/api/reactions.add', {
                headers: {
                    Authorization: `Bearer ${process.env.SLACK_ACCESS_TOKEN}`
                },
                json: {
                    token: process.env.SLACK_VERIFICATION_TOKEN,
                    name: response === 'ok' ? 'heavy_check_mark' : 'heavy_minus_sign',
                    channel: event.channel,
                    timestamp: event.ts
                }
            });
        }
    };
};
